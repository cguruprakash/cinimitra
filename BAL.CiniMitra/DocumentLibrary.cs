﻿using CiniMitra.DAL.DataObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;

namespace CiniMitra.BAL
{
    public class DocumentLibrary
    {
        public int AddToDocLibrary(HttpPostedFileBase httpPostedFile)
        {
            int addedDocId = 0;
            if (!string.IsNullOrEmpty(httpPostedFile.FileName.ToString()))
            {
                byte[] bytes;
                using (BinaryReader _br = new BinaryReader(httpPostedFile.InputStream))
                {
                    bytes = _br.ReadBytes(httpPostedFile.ContentLength);
                }
                
                string fileExt = Path.GetExtension(httpPostedFile.FileName.ToString());

                string fileName = Convert.ToString($"{Guid.NewGuid()}_{DateTime.Now.ToString("yyyyMMddHHmmss")}_{Path.GetFileName(httpPostedFile.FileName)}");
                DocumentLibraryDAO documentData = new DocumentLibraryDAO()
                {
                    DocFile = bytes,
                    DocName = fileName,
                    DocType = httpPostedFile.ContentType
                };
                DAL.DocumentLibrary documentLib = new DAL.DocumentLibrary();
                addedDocId = documentLib.AddToDocumentLibrary(documentData);
            }
            return addedDocId;
        }
    }
}
