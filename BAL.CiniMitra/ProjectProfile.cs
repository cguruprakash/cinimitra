﻿using CiniMitra.DAL.DataObjects;
using CiniMitra.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace CiniMitra.BAL
{
    public class ProjectProfile
    {
        public bool NewProjectProfile(ProjectProfileDAO projectProfile)
        {
            bool isAdded = false;
            if (projectProfile != null)
            {
                DAL.ProjectProfile projProf = new DAL.ProjectProfile();
                isAdded = projProf.AddProjectProfile(projectProfile);
            }
            return isAdded;
        }
        public List<FilmTypeDAO> GetFilmTypes()
        {
            DAL.ProjectProfile projectProfile = new DAL.ProjectProfile();
            return projectProfile.GetFilmTypes();
        }
    }
}
