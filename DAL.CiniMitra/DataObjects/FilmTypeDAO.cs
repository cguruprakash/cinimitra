﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CiniMitra.DAL.DataObjects
{
    public class FilmTypeDAO
    {
        public int ID { get; set; }
        public string FilmType { get; set; }
        public int IsActiveOrNot { get; set; }
    }
}
