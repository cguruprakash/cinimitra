﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace CiniMitra.DAL.DataObjects
{
    public class DocumentLibraryDAO
    {
        public int DocID { get; set; }
        public string DocName { get; set; }
        public byte[] DocFile { get; set; }
        public string DocType { get; set; }
        public int DocStatus { get; set; }
    }
}
