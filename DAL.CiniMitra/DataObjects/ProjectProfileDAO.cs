﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CiniMitra.DAL.DataObjects
{
    public class ProjectProfileDAO
    {
        public int ProdHouseID { get; set; }
        public string FilmName { get; set; }
        public string ProducerName { get; set; }
        public string ProducerMobile { get; set; }
        public string ProducerEmail { get; set; }
        public string LeadingArtists { get; set; }
        public string MovieOrigin { get; set; }
        public string FilmLang { get; set; }
        public string FilmDirName { get; set; }
        public string FilmCategory { get; set; }
        public string OriginalFilmName { get; set; }
        public string OriginalFilmLang { get; set; }
        public int FilmTypeId { get; set; }
        public string ProdHAddr { get; set; }
        public int FilmScriptId { get; set; }
        public int ProdHStatus { get; set; }
    }
}
