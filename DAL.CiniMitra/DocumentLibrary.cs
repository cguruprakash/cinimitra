﻿using CiniMitra.DAL.DataObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CiniMitra.DAL
{
    public class DocumentLibrary
    {
        private string conString = ConfigurationManager.ConnectionStrings["CONDBN"].ConnectionString.ToString();
        public int AddToDocumentLibrary(DocumentLibraryDAO documentObject)
        {
            int addedDocId = 0;
            SqlConnection con_ = null;
            if (documentObject != null)
            {
                try
                {
                    using (con_ = new SqlConnection())
                    {
                        con_.ConnectionString = conString;
                        if (con_.State == ConnectionState.Closed) con_.Open();
                        using (SqlCommand cmd_ = new SqlCommand())
                        {
                            cmd_.CommandText = "SP_AddDocument";
                            cmd_.CommandType = CommandType.StoredProcedure;
                            SqlParameter docNameParam = new SqlParameter()
                            {
                                ParameterName = "@documentName",
                                SqlDbType = SqlDbType.VarChar,
                                Size = -1,
                                Value = documentObject.DocName
                            };
                            cmd_.Parameters.Add(docNameParam);
                            SqlParameter docFileParam = new SqlParameter()
                            {
                                ParameterName = "@documentFile",
                                SqlDbType = SqlDbType.VarBinary,
                                Value = documentObject.DocFile
                            };
                            cmd_.Parameters.Add(docFileParam);
                            SqlParameter docTypeParam = new SqlParameter()
                            {
                                ParameterName = "@documentType",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 200,
                                Value = documentObject.DocType
                            };
                            cmd_.Parameters.Add(docTypeParam);
                            SqlParameter insertedDocIdParam = new SqlParameter()
                            {
                                ParameterName = "@documentId",
                                SqlDbType = SqlDbType.Int,
                                Direction = ParameterDirection.Output
                            };
                            cmd_.Parameters.Add(insertedDocIdParam);
                            cmd_.Connection = con_;
                            cmd_.ExecuteNonQuery();
                            addedDocId = Convert.ToInt32(cmd_.Parameters["@documentId"].Value);
                        }
                    }
                }
                catch (Exception err)
                {
                    //add to log
                    throw err;
                }
                finally
                {
                    con_.Close();
                    con_.Dispose();
                }
            }
            return addedDocId;
        }
    }
}
