﻿using CiniMitra.DAL.DataObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CiniMitra.DAL
{
    public class ProjectProfile
    {
        private string conString = ConfigurationManager.ConnectionStrings["CONDBN"].ConnectionString.ToString();
        public bool AddProjectProfile(ProjectProfileDAO projectProfile)
        {
            bool isAdded = false;
            SqlConnection con_ = null;
            if (projectProfile != null)
            {
                try
                {
                    using (con_ = new SqlConnection())
                    {
                        con_.ConnectionString = conString;
                        if (con_.State == ConnectionState.Closed) con_.Open();
                        using (SqlCommand cmd_ = new SqlCommand())
                        {
                            cmd_.Connection = con_;
                            cmd_.CommandText = "SP_NewProjectProfile";
                            cmd_.CommandType = CommandType.StoredProcedure;
                            SqlParameter filmNameParam = new SqlParameter()
                            {
                                ParameterName = "@FilmName",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 100,
                                Value = projectProfile.FilmName.ToString()
                            };
                            cmd_.Parameters.Add(filmNameParam);
                            SqlParameter producerNameParam = new SqlParameter()
                            {
                                ParameterName = "@ProducerName",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 100,
                                Value = projectProfile.ProducerName.ToString()
                            };
                            cmd_.Parameters.Add(producerNameParam);
                            SqlParameter producerMoblParam = new SqlParameter()
                            {
                                ParameterName = "@ProducerMobile",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 25,
                                Value = projectProfile.ProducerMobile
                            };
                            cmd_.Parameters.Add(producerMoblParam);
                            SqlParameter producerEmailParam = new SqlParameter()
                            {
                                ParameterName = "@ProducerEmail",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 25,
                                Value = projectProfile.ProducerEmail.ToString()
                            };
                            cmd_.Parameters.Add(producerEmailParam);
                            SqlParameter leadArtistParam = new SqlParameter()
                            {
                                ParameterName = "@LeadingArtists",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 500,
                                Value = projectProfile.LeadingArtists.ToString()
                            };
                            cmd_.Parameters.Add(leadArtistParam);
                            SqlParameter originMovieParam = new SqlParameter()
                            {
                                ParameterName = "@MovieOrigin",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 100,
                                Value = projectProfile.MovieOrigin.ToString()
                            };
                            cmd_.Parameters.Add(originMovieParam);
                            SqlParameter filmLangParam = new SqlParameter()
                            {
                                ParameterName = "@FilmLang",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 100,
                                Value = projectProfile.FilmLang.ToString()
                            };
                            cmd_.Parameters.Add(filmLangParam);
                            SqlParameter directorNameParam = new SqlParameter()
                            {
                                ParameterName = "@DirectorName",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 100,
                                Value = projectProfile.FilmDirName.ToString()
                            };
                            cmd_.Parameters.Add(directorNameParam);
                            SqlParameter filmCatgyParam = new SqlParameter()
                            {
                                ParameterName = "@MovieCategory",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 100,
                                Value = projectProfile.FilmCategory.ToString()
                            };
                            cmd_.Parameters.Add(filmCatgyParam);
                            SqlParameter originalFilmNameParam = new SqlParameter()
                            {
                                ParameterName = "@OriginalFilmName",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 100,
                                Value = projectProfile.OriginalFilmName
                            };
                            cmd_.Parameters.Add(originalFilmNameParam);
                            SqlParameter originalFilmLangParam = new SqlParameter()
                            {
                                ParameterName = "@OriginalFilmLang",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 100,
                                Value = projectProfile.OriginalFilmLang
                            };
                            cmd_.Parameters.Add(originalFilmLangParam);
                            SqlParameter filmTypeParam = new SqlParameter()
                            {
                                ParameterName = "@FilmType",
                                SqlDbType = SqlDbType.Int,
                                Value = Convert.ToInt32(projectProfile.FilmTypeId)
                            };
                            cmd_.Parameters.Add(filmTypeParam);
                            SqlParameter productionHouseAddrParam = new SqlParameter()
                            {
                                ParameterName = "@ProductionHouseAddr",
                                SqlDbType = SqlDbType.VarChar,
                                Size = 500,
                                Value = projectProfile.ProdHAddr.ToString()
                            };
                            cmd_.Parameters.Add(productionHouseAddrParam);
                            SqlParameter filmScriptParam = new SqlParameter()
                            {
                                ParameterName = "@FilmScript",
                                SqlDbType = SqlDbType.Int,
                                Value = Convert.ToInt32(projectProfile.FilmScriptId)
                            };
                            cmd_.Parameters.Add(filmScriptParam);

                            cmd_.ExecuteNonQuery();
                            isAdded = true;
                        }
                    }
                }
                catch (Exception err)
                {
                    //add to log
                    throw err;
                }
                finally
                {
                    con_.Close();
                    con_.Dispose();
                }
            }
            return isAdded;
        }
        public List<FilmTypeDAO> GetFilmTypes()
        {
            List<FilmTypeDAO> filmTypeList = null;
            SqlConnection con_ = null;
            try
            {
                using (con_ = new SqlConnection())
                {
                    con_.ConnectionString = conString;
                    if (con_.State == ConnectionState.Closed) con_.Open();
                    using (SqlCommand cmd_ = new SqlCommand())
                    {
                        cmd_.Connection = con_;
                        cmd_.CommandTimeout = 100000;
                        cmd_.CommandText = "SP_GetFilmType";
                        cmd_.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader _rdr = cmd_.ExecuteReader())
                        {
                            if (_rdr.HasRows != false)
                            {
                                filmTypeList = new List<FilmTypeDAO>();
                                while (_rdr.Read())
                                {
                                    FilmTypeDAO filmType = new FilmTypeDAO
                                    {
                                        FilmType = _rdr["FilmType"].ToString(),
                                        ID = Convert.ToInt32(_rdr["ID"])
                                    };
                                    filmTypeList.Add(filmType);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                //add to log
                throw err;
            }
            finally
            {
                con_.Close();
                con_.Dispose();
            }
            return filmTypeList;
        }
    }
}
