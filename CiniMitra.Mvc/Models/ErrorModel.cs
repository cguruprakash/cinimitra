﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CiniMitra.Mvc.Models
{
    public class ErrorModel
    {
        public int HttpStatusCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}