﻿using CiniMitra.DAL.DataObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CiniMitra.Mvc.Models
{
    public class ProjectProfileModel {

        [Key]
        public int ProdHouseID { get; set; }

        [Required(ErrorMessage ="Please enter name of the film.")]
        [Display(Name = "Name of the Film")]
        [StringLength(maximumLength:100,ErrorMessage ="Film name must be within 100 characters.")]
        public string FilmName { get; set; }

        [Required(ErrorMessage = "Please enter name of the Producer.")]
        [Display(Name = "Name of the Producer")]
        [StringLength(maximumLength: 100, ErrorMessage = "Producer name must be within 100 characters.")]
        public string ProducerName { get; set; }

        [Display(Name = "Producer Mobile Number")]
        [StringLength(maximumLength: 25, ErrorMessage = "Mobile number must be within 25 characters.")]
        public string ProducerMobile { get; set; }

        [Required(ErrorMessage = "Please enter e-mail address of the producer.")]
        [Display(Name = "Producer E-mail Address")]
        [DataType(DataType.EmailAddress,ErrorMessage ="Please enter a valid E-mail address.")]
        [StringLength(maximumLength: 25, ErrorMessage = "Producer name must be within 25 characters.")]
        public string ProducerEmail { get; set; }

        [Required(ErrorMessage = "Please enter name of the leading Artists.")]
        [Display(Name = "Name of Leading Artists")]
        [StringLength(maximumLength: 500, ErrorMessage = "Names of the leading artists must be within 500 characters.")]
        public string LeadingArtists { get; set; }

        [Required(ErrorMessage = "Please choose/enter an origin of the movie.")]
        [Display(Name = "Origin of Movie")]
        [StringLength(maximumLength: 100, ErrorMessage = "max. of 100 characters are allowed.")]
        public string MovieOrigin { get; set; }

        [Required(ErrorMessage = "Please choose/enter the language of the movie.")]
        [Display(Name = "Language of the film")]
        [StringLength(maximumLength: 100, ErrorMessage = "Language of the movie must be within 100 characters.")]
        public string FilmLang { get; set; }

        [Required(ErrorMessage = "Please enter the name of the Director.")]
        [Display(Name = "Name of the Director")]
        [StringLength(maximumLength: 100, ErrorMessage = "Director's name must be within 100 characters.")]
        public string FilmDirName { get; set; }

        [Required(ErrorMessage = "Please choose a category suits the movie.")]
        [Display(Name = "Movie Category")]
        public string FilmCategory { get; set; }

        //[Required(ErrorMessage = "Please enter the original name of the film.")]
        [Display(Name = "Original Movie Name")]
        [StringLength(maximumLength: 100, ErrorMessage = "Original name of the film must be within 100 characters.")]
        public string OriginalFilmName { get; set; }

        //[Required(ErrorMessage = "Please enter the original language of the film.")]
        [Display(Name = "Original Movie Language")]
        [StringLength(maximumLength: 100, ErrorMessage = "Original language of the film must be within 100 characters.")]
        public string OriginalFilmLang { get; set; }

        [Required(ErrorMessage = "Please choose a type of the film.")]
        [Display(Name = "Type of the film")]
        public int? FilmTypeId { get; set; }

        public List<FilmTypeDAO> FilmTypeList { get; set; }

        [Required(ErrorMessage = "Please enter the name and address of the Production house.")]
        [Display(Name = "Name and Address of the Production House")]
        [StringLength(maximumLength: 100, ErrorMessage = "Original name of the film must be within 100 characters.")]
        public string ProdHAddr { get; set; }

        [Required(ErrorMessage = "Please upload a script file of the movie.")]
        [Display(Name = "Upload Script file")]
        //[RegularExpression(pattern: @"([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.png|.jpg|.jpeg)$", ErrorMessage ="*.pdf,*.jpg,*.jpeg,*.png are allowable formats.")]
        public HttpPostedFileBase FilmScript { get; set; }

        //[NotMapped]
        //public int FilmScriptId { get; set; }
        //[NotMapped]
        //public int ProdHStatus { get; set; }
    }
}