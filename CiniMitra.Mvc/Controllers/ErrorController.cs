﻿using CiniMitra.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CiniMitra.Mvc.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index(int statusCode)
        {
            ErrorModel errorModel = null;
            if (statusCode > 0)
            {
                errorModel = new ErrorModel()
                {
                    HttpStatusCode = statusCode,
                    ErrorMessage = GetErrText(statusCode)
                };
            }
            else
            {
                errorModel = new ErrorModel()
                {
                    HttpStatusCode = 500,
                    ErrorMessage = "Internal Server Error"
                };
            }
            return View(errorModel);
        }
        private string GetErrText(int statusCode)
        {
            string errTxt = "Internal Server Error";
            if (statusCode > 0)
            {
                switch (statusCode) {
                    case 100: errTxt = "Continue"; break;
                    case 101: errTxt = "Switching Protocols"; break;
                    case 200: errTxt = "OK"; break;
                    case 201: errTxt = "Created"; break;
                    case 202: errTxt = "Accepted"; break;
                    case 203: errTxt = "Non-Authoritative Information"; break;
                    case 204: errTxt = "No Content"; break;
                    case 205: errTxt = "Reset Content"; break;
                    case 206: errTxt = "Partial Content"; break;
                    case 300: errTxt = "Multiple Choices"; break;
                    case 301: errTxt = "Moved Permanently"; break;
                    case 302: errTxt = "Moved Temporarily"; break;
                    case 303: errTxt = "See Other"; break;
                    case 304: errTxt = "Not Modified"; break;
                    case 305: errTxt = "Use Proxy"; break;
                    case 400: errTxt = "Bad Request"; break;
                    case 401: errTxt = "Unauthorized"; break;
                    case 402: errTxt = "Payment Required"; break;
                    case 403: errTxt = "Forbidden"; break;
                    case 404: errTxt = "Not Found"; break;
                    case 405: errTxt = "Method Not Allowed"; break;
                    case 406: errTxt = "Not Acceptable"; break;
                    case 407: errTxt = "Proxy Authentication Required"; break;
                    case 408: errTxt = "Request Time-out"; break;
                    case 409: errTxt = "Conflict"; break;
                    case 410: errTxt = "Gone"; break;
                    case 411: errTxt = "Length Required"; break;
                    case 412: errTxt = "Precondition Failed"; break;
                    case 413: errTxt = "Request Entity Too Large"; break;
                    case 414: errTxt = "Request-URI Too Large"; break;
                    case 415: errTxt = "Unsupported Media Type"; break;
                    case 500: errTxt = "Internal Server Error"; break;
                    case 501: errTxt = "Not Implemented"; break;
                    case 502: errTxt = "Bad Gateway"; break;
                    case 503: errTxt = "Service Unavailable"; break;
                    case 504: errTxt = "Gateway Time-out"; break;
                    case 505: errTxt = "HTTP Version not supported"; break;
                    default:
                        errTxt = "Internal Server Error";
                    break;
                }
            }
            return errTxt;

        }
    }
}