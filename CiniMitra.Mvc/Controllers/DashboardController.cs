﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CiniMitra.Mvc.Controllers
{
    [RoutePrefix("~/Dashboard")]
    public class DashboardController : Controller
    {
        [Route("~/Dashboard")]
        // GET: Dashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}