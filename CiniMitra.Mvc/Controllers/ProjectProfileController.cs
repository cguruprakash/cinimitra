﻿using CiniMitra.BAL;
using CiniMitra.DAL.DataObjects;
using CiniMitra.Mvc.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CiniMitra.Mvc.Controllers
{
    //[Authorize]
    public class ProjectProfileController : Controller
    {
        // GET: ProjectProfile
        public ActionResult Index()
        {
            ProjectProfileModel projProfileModel = new ProjectProfileModel();
            projProfileModel.FilmTypeList = this.FilmTypeList();
            return View(projProfileModel);
        }

        [HttpPost]
        public ActionResult Index(ProjectProfileModel projectprofile)
        {
            ProjectProfileModel projProfileModel = new ProjectProfileModel();
            projProfileModel.FilmTypeList = this.FilmTypeList();

            string producerMobile = (projectprofile.ProducerMobile != null || projectprofile.ProducerMobile != "") ? projectprofile.ProducerMobile : string.Empty;
            string originalFilmName = (projectprofile.OriginalFilmName != null || projectprofile.OriginalFilmName != "") ? projectprofile.OriginalFilmName : string.Empty;
            string originalFilmLang = (projectprofile.OriginalFilmLang != null || projectprofile.OriginalFilmLang != "") ? projectprofile.OriginalFilmLang : string.Empty;

            if (!string.IsNullOrEmpty(projectprofile.FilmCategory))
            {
                if (projectprofile.FilmCategory.ToLower() == "remake film")
                {
                    if (string.IsNullOrEmpty(projectprofile.OriginalFilmName) || string.IsNullOrEmpty(projectprofile.OriginalFilmLang))
                    {
                        if (string.IsNullOrEmpty(projectprofile.OriginalFilmName))
                        {
                            ModelState.AddModelError("OriginalFilmName", "Please enter the original name of the film.");
                        }
                        if (string.IsNullOrEmpty(projectprofile.OriginalFilmLang))
                        {
                            ModelState.AddModelError("OriginalFilmLang", "Please enter the original language of the film.");
                        }
                        return View(projProfileModel);
                    }
                    else
                    {
                        originalFilmName = projectprofile.OriginalFilmName.ToString();
                        originalFilmLang = projectprofile.OriginalFilmLang.ToString();
                    }
                } 
            }
            if (projectprofile.FilmTypeId == null)
            {
                ModelState.AddModelError("FilmTypeId", "Please choose a type of the film.");
                return View(projProfileModel);
            }
            if (projectprofile.FilmScript != null)
            {
                if (projectprofile.FilmScript.ContentLength > 1 * 1024 * 1024)
                {
                    ModelState.AddModelError("FilmScript", "The script file must be max. of 1MB size.");
                    return View(projProfileModel);
                } 
            }
            try
            {
                projectprofile.ProducerMobile = producerMobile;
                projectprofile.OriginalFilmName = originalFilmName;
                projectprofile.OriginalFilmLang = originalFilmLang;
                projectprofile.FilmTypeList = null;
                
                if (ModelState.IsValid)
                 {
                     DocumentLibrary document = new DocumentLibrary();
                     int addedDocId = document.AddToDocLibrary(projectprofile.FilmScript);
                     ProjectProfile projProfile = new ProjectProfile();
                     ProjectProfileDAO ppObj = new ProjectProfileDAO()
                     {
                         FilmName = projectprofile.FilmName.ToString(),
                         ProducerName = projectprofile.ProducerName.ToString(),
                         ProducerEmail = projectprofile.ProducerEmail.ToString(),
                         ProducerMobile = projectprofile.ProducerMobile,
                         LeadingArtists = projectprofile.LeadingArtists.ToString(),
                         MovieOrigin = projectprofile.MovieOrigin.ToString(),
                         FilmLang = projectprofile.FilmLang.ToString(),
                         FilmDirName = projectprofile.FilmDirName.ToString(),
                         FilmCategory = projectprofile.FilmCategory.ToString(),
                         OriginalFilmName = projectprofile.OriginalFilmName,
                         OriginalFilmLang = projectprofile.OriginalFilmLang,
                         FilmTypeId = Convert.ToInt32(projectprofile.FilmTypeId),
                         ProdHAddr = projectprofile.ProdHAddr.ToString(),
                         FilmScriptId = Convert.ToInt32(addedDocId)
                     };
                     bool isAdded = projProfile.NewProjectProfile(ppObj);
                     ViewBag.AddStatus = isAdded;
                 }
            }
            catch (Exception){
                ViewBag.AddStatus = false;
            }
            
            return View(projProfileModel);
        }

        private List<FilmTypeDAO> FilmTypeList()
        {
            BAL.ProjectProfile projectProfile = new BAL.ProjectProfile();
            return projectProfile.GetFilmTypes();
        }
    }
}